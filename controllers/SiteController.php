<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Noticias;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionInicio()
    {
       $dataProvider = new ActiveDataProvider([
           'query' => Noticias::find(),
           'pagination' => [
                'pageSize' => 20,
            ],
       ]);
        return $this->render('index', [
            'datos' => $dataProvider,
        ]);
    }

    public function actionPagina1()
    {
        /*
         * 1er metodo
         * Realizada la consulta en el controlador
         */
        //$total=Noticias::find()->count();
        //$registro= Noticias::find()->offset(rand(0, $total-1))->limit(1)->one();
        
        
        /*
         * 2do metodo
         * Realizada la consulta en el modelo Noticias a traves de una 
         * funcion 'mostrarUna()' que llamamos desde el controlador
         */
        
        $registro= Noticias::mostrarUna();
        
        return $this->render("pagina1",[
            "dato"=>$registro,
        ]);
    }
    
    public function actionPagina2()
    {
        return $this->render('pagina2');
    }
    
    public function actionPagina3()
    {
        return $this->render('pagina3');
    }
}
