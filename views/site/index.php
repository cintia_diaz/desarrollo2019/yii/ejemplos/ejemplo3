<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Ejemplo 1 de aplicacion</h1>

        <p>Podemos ver un ejemplo del funcionamiento de este framework.</p>
        
        
    </div>

    <div class="body-content">  
        <div class="row">
      <?php 
        echo ListView::widget([
            'dataProvider' => $datos,
            'itemView' => '_post',
        ]);
      ?>
         </div>
    </div>
</div>


<?=
        $this->render('_modal',[
            "titulo"=>"Pagina de inicio",
            "texto"=>"accion Inicio",
            "boton"=>"cerrar",
        ])

?>