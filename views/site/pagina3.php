<?php

use yii\helpers\Html;

/*
$this->title = 'Pagina 3';
$this->params['breadcrumbs'][] = $this->title;
*/
/*
 * Añadir codigo JS en una vista para no cargarlo en AppAsset
 */
$this->registerJsFile(
        '@web/js/video.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
        );
?>

<div class="center-block embed-responsive embed-responsive-16by9">
    <video class="embed-responsive-item" controls="">
        <source src="<?= Yii::getAlias("@web") . "/video/Forest.mp4"?>" type="video/mp4">
    </video>
</div>

<!--
 embed-responsive: contenedor para contenido incrustrado, lo escala en cualquier dispositivo
 embed-responsive-16by9: crea un aspecto de radio 16:9 para contenido incrustrado
 embed-responsive-item: dentro de embed-responsive, escala dentro del padre
-->

<div class="center-block embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tlyomP5dawY" frameborder="0" allow="autoplay; encrypted-media;" allowfullscreen></iframe>    
</div>