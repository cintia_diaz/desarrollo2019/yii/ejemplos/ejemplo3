<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use app\models\Noticias;


$this->title = 'Pagina 1';
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <?=
    Html::img("@web/imgs/foto1.jpg", 
            ['class'=> 'img-rounded img-responsive center-block foto1',
             'alt' => 'Una foto fija',
            ]);
      
    ?> 
</div>
<div>
    <div class="center-block">
        <h1><?= $dato -> titulo; ?></h1>
        <p><?= $dato-> texto; ?></p>
    </div>
</div>

<?= $this->render('_modal',[
    "titulo"=>"Pagina 1",
    "texto"=>"accion Pagina 1",
    "boton"=>"cerrar",
]);?>